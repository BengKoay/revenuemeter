const axios = require('axios');

const cron = require('node-cron');
const moment = require('moment');

const ModbusRTU = require('modbus-serial');

const axiosSolar = {
	baseURL: 'https://5l68pc71bd.execute-api.ap-southeast-1.amazonaws.com/',
	authorization: '71b65c7a-6097-405c-9fa7-66ef64d651cc',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: { Authorization: axiosSolar.authorization },
});

const deviceList = [
	{
		ref: 1,
		deviceId: '69fe0303-95f1-42c0-a91a-614d54066c54', // Revenue Meter Building ABC
	},
];

let client = new ModbusRTU();

client.connectRTUBuffered(
	'/dev/ttyAMA0',
	{ baudRate: 9600 },
	//   { baudRate: 38400, stopBits: 2 }, //  baudRate = 38400, to be changed to 9600 (standard), 1-start, 8-data, N-parity, 2-stop
	function (error, success) {
		if (error) {
			console.log('Serial Port initialization unsuccessful');
		} else {
			console.log('Serial port initialization successful');
		}
	}
);
const main = async () => {
	cron.schedule(`50 * * * * *`, async () => {
		for (const eachDevice of deviceList) {
			let payload = {
				activeFwdEnergy: 0,
				activeRevEnergy: 0,
				totalActivePower: 0,
				voltage1: 0,
				voltage2: 0,
				voltage3: 0,
				currentN: 0,
				current1: 0,
				current2: 0,
				current3: 0,
				powerFactor: 0,
				frequency: 0,
				timestamp: 0,
				deviceId: '',
				ref: 0,
			};
			try {
				// read modbus
				await client.setID(eachDevice.ref);
				await new Promise((resolve) => setTimeout(resolve, 100));

				let energyMeterReading = await client.readHoldingRegisters('9044', 4);
				await new Promise((resolve) => setTimeout(resolve, 100));

				energyMeterReading.buffer.swap16();
				payload.activeFwdEnergy = energyMeterReading.buffer.readFloatLE(); // 9045
				payload.activeRevEnergy = energyMeterReading.buffer.readFloatLE(4); // 9047
				let voltageCurrentReading = await client.readHoldingRegisters(
					'0021',
					12
				);
				await new Promise((resolve) => setTimeout(resolve, 100));
				payload.voltage1 = voltageCurrentReading.buffer.readFloatBE(0); // 0021
				payload.voltage2 = voltageCurrentReading.buffer.readFloatBE(4); // 0021
				payload.voltage3 = voltageCurrentReading.buffer.readFloatBE(8); // 0025
				payload.current1 = voltageCurrentReading.buffer.readFloatBE(12); // 0027
				payload.current2 = voltageCurrentReading.buffer.readFloatBE(16); // 0029
				payload.current3 = voltageCurrentReading.buffer.readFloatBE(20); // 0031
				let activePowerReading = await client.readHoldingRegisters('0035', 16);
				await new Promise((resolve) => setTimeout(resolve, 100));
				payload.totalActivePower =
					activePowerReading.buffer.readFloatBE(0) / 1000; // 0035
				payload.powerFactor = activePowerReading.buffer.readFloatBE(12); // 0041;
				payload.frequency = activePowerReading.buffer.readFloatBE(28); // 0049
			} catch (e) {}
			payload.timestamp = moment().unix();
			payload.deviceId = eachDevice.deviceId;
			payload.ref = eachDevice.ref;
			try {
				// send via REST
				let sendPayload = JSON.stringify(payload);
				const res = await axiosMainHelper.post('devicePayload', sendPayload);
				console.log(
					'res.status: ',
					res.status,
					'res.statusText: ',
					res.statusText
				);
				console.log(res.config.data);
			} catch (e) {
				console.log(e);
			}
		}
	});
};

main();
