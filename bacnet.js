const bacnet = require('bacstack');

// Initialize BACStack
const client = new bacnet({ apduTimeout: 100 });

// Read Device Object
const main = async () => {
	let result = {
		activeEnergy: 0,
		negativeActiveEnergy: 0,
		// activeEnergy: 0,
		activePower: 0,
		apparentEnergy: 0,
		negativeReactiveEnergy: 0,
		reactiveEnergy: 0,
		apparentPower: 0,
		reactivePower: 0,
		powerFactor: 0,
		frequency: 0,
		current1: 0,
		current2: 0,
		current3: 0,
		voltage12: 0,
		voltage23: 0,
		voltage31: 0,
		voltage1: 0,
		voltage2: 0,
		voltage3: 0,
		power1: 0,
		power2: 0,
		power3: 0,
		reactivePower1: 0,
		reactivePower2: 0,
		reactivePower3: 0,
	};
	//   const requestArray = [
	//     {
	//       objectId: { type: 2, instance: 135 },
	//       properties: [{ id: 85 }],
	//     },
	//   ];
	//   let volt = await client.readPropertyMultiple('10.10.50.1', requestArray);
	//   console.log('volt');

	//   setTimeout(
	await new Promise((resolve) => {
		client.readProperty(
			'10.10.50.1',
			{ type: 2, instance: 135 },
			85,
			(err, value) => {
				if (value) {
					result.activePower = value.values[0].value;
					resolve(value);
				}
			}
		);
	});
	await new Promise((resolve) => {
		client.readProperty(
			'10.10.50.1',
			{ type: 2, instance: 175 },
			85,
			(err, value) => {
				if (value) {
					result.activeEnergy = value.values[0].value;
					resolve(value);
				}
			}
		);
	});
	//     0
	//   );

	// await client.readProperty(
	//   '10.10.50.1',
	//   { type: 2, instance: 135 },
	//   85,
	//   (err, value) => {
	//     result.activePower = value.values[0].value;
	//     console.log('value: ', value.values[0].value);
	//   }
	// );
	console.log(result);
	//   await client.readPropertyMultiple(
	//     '10.10.50.1',
	//     requestArray,
	//     (err, value) => {
	//       console.log('value: ', value.values[0].values[0].value[0].value);
	//       //   console.log('value: ', value);
	//       //   for (const eachValue of value.values[0].values) {
	//       //     // console.log(eachValue);
	//       //     if (eachValue.id == 85) {
	//       //       for (const eachProp of eachValue.value) {
	//       //         console.log(eachProp.value);
	//       //       }
	//       //     }
	//       //   }
	//     }
	//   );
};

setInterval(() => main(), 1000);
