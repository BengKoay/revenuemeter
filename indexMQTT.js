const awsIot = require('aws-iot-device-sdk');
// const fs = require('fs-extra');
const ModbusRTU = require('modbus-serial');
const cron = require('node-cron');
const moment = require('moment');
const piConfig = require('./piConfig.js').piConfig;
// const appProperties = require('../helper/config/properties');
const awsIoTHost = 'a3j1bzslspz95c-ats.iot.ap-southeast-1.amazonaws.com';
// const deviceId = '36424aa2-d9c5-4aaf-9a51-1c6d718bfe29';

const clientId = `${piConfig.lotId.replace(
	/-/g,
	''
)}_${piConfig.deviceId.replace(/-/g, '')}`;
// const dataPool = `${process.cwd()}/localdata/dataPool/processedData`;
// const publishingPath = `${dataPool}/publishing`;
// const publishedPath = `${dataPool}/published`;
// const readingTopic = `publish/lot_reading`;
const topic = 'iot/gabi_reading';
const subscribeTopicPayload = `iot/gabi_write/${piConfig.lotId}`;
const publishingTopic = topic;
const subscribeTopic = subscribeTopicPayload;

const pubOpts = { qos: 1 };

const device = awsIot.device({
	clientId: clientId,
	// host: appProperties.awsIoTHost,
	host: awsIoTHost,
	offlineQueueing: false,
	clientCert: Buffer.from(piConfig.awsIoTCredentials.clientCert),
	privateKey: Buffer.from(piConfig.awsIoTCredentials.privateKey),
	caCert: Buffer.from(
		`-----BEGIN CERTIFICATE-----\nMIIDQTCCAimgAwIBAgITBmyfz5m/jAo54vB4ikPmljZbyjANBgkqhkiG9w0BAQsF\nADA5MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRkwFwYDVQQDExBBbWF6\nb24gUm9vdCBDQSAxMB4XDTE1MDUyNjAwMDAwMFoXDTM4MDExNzAwMDAwMFowOTEL\nMAkGA1UEBhMCVVMxDzANBgNVBAoTBkFtYXpvbjEZMBcGA1UEAxMQQW1hem9uIFJv\nb3QgQ0EgMTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALJ4gHHKeNXj\nca9HgFB0fW7Y14h29Jlo91ghYPl0hAEvrAIthtOgQ3pOsqTQNroBvo3bSMgHFzZM\n9O6II8c+6zf1tRn4SWiw3te5djgdYZ6k/oI2peVKVuRF4fn9tBb6dNqcmzU5L/qw\nIFAGbHrQgLKm+a/sRxmPUDgH3KKHOVj4utWp+UhnMJbulHheb4mjUcAwhmahRWa6\nVOujw5H5SNz/0egwLX0tdHA114gk957EWW67c4cX8jJGKLhD+rcdqsq08p8kDi1L\n93FcXmn/6pUCyziKrlA4b9v7LWIbxcceVOF34GfID5yHI9Y/QCB/IIDEgEw+OyQm\njgSubJrIqg0CAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMC\nAYYwHQYDVR0OBBYEFIQYzIU07LwMlJQuCFmcx7IQTgoIMA0GCSqGSIb3DQEBCwUA\nA4IBAQCY8jdaQZChGsV2USggNiMOruYou6r4lK5IpDB/G/wkjUu0yKGX9rbxenDI\nU5PMCCjjmCXPI6T53iHTfIUJrU6adTrCC2qJeHZERxhlbI1Bjjt/msv0tadQ1wUs\nN+gDS63pYaACbvXy8MWy7Vu33PqUXHeeE6V/Uq2V8viTO96LXFvKWlJbYK8U90vv\no/ufQJVtMVT8QtPHRh8jrdkPSHCa2XV4cdFyQzR1bldZwgJcJmApzyMZFo6IQ6XU\n5MsI+yMRQ+hDKXJioaldXgjUkK642M4UwtBV8ob2xJNDd2ZhwLnoQdeXeGADbkpy\nrqXRfboQnoZsG4q5WTP468SQvvG5\n-----END CERTIFICATE-----\n`
	),
});

let client = new ModbusRTU();

client.connectRTUBuffered(
	'/dev/ttyAMA0',
	{ baudRate: 9600 },
	//   { baudRate: 38400, stopBits: 2 }, //  baudRate = 38400, to be changed to 9600 (standard), 1-start, 8-data, N-parity, 2-stop
	function (error, success) {
		if (error) {
			console.log('Serial Port initialization unsuccessful');
		} else {
			console.log('Serial port initialization successful');
		}
	}
);

const dataToPush = { data: [] };
// const dataToPush = {
// 	data: [
// 		{
// 			activeFwdEnergy: 0,
// 			activeRevEnergy: 0,
// 			totalActivePower: 0,
// 			voltage1: 0,
// 			voltage2: 0,
// 			voltage3: 0,
// 			currentN: 0,
// 			current1: 0,
// 			current2: 0,
// 			current3: 0,
// 			powerFactor: 0,
// 			frequency: 0,
// 			timestamp: 0,
// 			// wf: 0,
// 			// int: 0,
// 			// aws: 0,
// 			deviceId: deviceId,
// 			ref: 5,
// 		},
// 	],
// };
const deviceList = [
	{
		ref: 1,
		deviceId: '40733647-2ca7-4e68-a348-5b0d8c074854', // Revenue Meter Building H
	},
];
const main = async () => {
	device.on('connect', function () {
		// device.subscribe(subscribeTopic);
		console.log('awsIoT connected');
	});

	device.on('close', function () {
		console.log('awsIoT disconnected');
		isDeviceConnected = false;
		reportedOnline = false;
	});

	device.on('error', function () {
		console.error('awsIoT error', arguments);
	});

	device.on('reconnect', function () {
		console.log('awsIoT reconnect...');
		isDeviceConnected = true;
	});

	device.on('timeout', function () {
		console.log('awsIoT timeout', arguments);
		isDeviceConnected = false;
		reportedOnline = false;
	});

	read(50);

	// cron.schedule(`0 * * * * *`, async () => {
	// 	published();
	// });
};

const published = async () => {
	{
		new Promise((resolve) => {
			device.publish(
				publishingTopic,
				// dataToPush,
				JSON.stringify(dataToPush),
				pubOpts,
				(err, data) => {
					if (err === null) {
						return resolve(true);
					}
					return resolve(false);
				}
			);
		});
		// result.length = 0;
	}
};

const read = async (time) => {
	cron.schedule(`${time} * * * * *`, async () => {
		const result = [];
		// setInterval(function () {
		//   console.log(deviceList);
		for (const eachDevice of deviceList) {
			let payload = {
				activeFwdEnergy: 0,
				activeRevEnergy: 0,
				totalActivePower: 0,
				voltage1: 0,
				voltage2: 0,
				voltage3: 0,
				currentN: 0,
				current1: 0,
				current2: 0,
				current3: 0,
				powerFactor: 0,
				frequency: 0,
				timestamp: 0,
				deviceId: '',
				ref: 0,
			};
			console.log(eachDevice);
			await client.setID(eachDevice.ref);
			await new Promise((resolve) => setTimeout(resolve, 100));
			try {
				let energyMeterReading = await client.readHoldingRegisters('9045', 4);
				await new Promise((resolve) => setTimeout(resolve, 100));
				payload.activeFwdEnergy = energyMeterReading.buffer.readFloatBE(); // 9045
				payload.activeRevEnergy = energyMeterReading.buffer.readFloatBE(4); // 9047
				let voltageCurrentReading = await client.readHoldingRegisters(
					'0021',
					12
				);
				await new Promise((resolve) => setTimeout(resolve, 100));
				payload.voltage1 = voltageCurrentReading.buffer.readFloatBE(0); // 0021
				payload.voltage2 = voltageCurrentReading.buffer.readFloatBE(4); // 0021
				payload.voltage3 = voltageCurrentReading.buffer.readFloatBE(8); // 0025
				payload.current1 = voltageCurrentReading.buffer.readFloatBE(12); // 0027
				payload.current2 = voltageCurrentReading.buffer.readFloatBE(16); // 0029
				payload.current3 = voltageCurrentReading.buffer.readFloatBE(20); // 0031
				let activePowerReading = await client.readHoldingRegisters('0035', 16);
				await new Promise((resolve) => setTimeout(resolve, 100));
				payload.totalActivePower =
					activePowerReading.buffer.readFloatBE(0) / 1000; // 0035
				payload.powerFactor = activePowerReading.buffer.readFloatBE(12); // 0041;
				payload.frequency = activePowerReading.buffer.readFloatBE(28); // 0049
				payload.timestamp = moment().unix();
				payload.deviceId = eachDevice.deviceId;
				payload.ref = eachDevice.ref;
				// console.log('payload', payload);
				// dataToPush.data.push(payload);
				result.push(payload);
				// console.log('result', result);
				// activeFwdEnergy: 0,
				// activeRevEnergy: 0,
				// totalActivePower: 0,
				// voltage1: 0,
				// voltage2: 0,
				// voltage3: 0,
				// currentN: 0,
				// current1: 0,
				// current2: 0,
				// current3: 0,
				// powerFactor: 0,
				// frequency: 0,
			} catch (e) {
				console.log(e);
			}
		}
		dataToPush.data = result;
		console.log(dataToPush);
		published();
	});
};

main();

// {
//   "deviceId": "e8219dfd-e549-498d-9258-7d99fb2b4870",
//   "ref": 1,
//   "lotId": "83ae72b8-3003-4789-a8ab-053eb7cfd432",
//   "newValue": 19
// }
